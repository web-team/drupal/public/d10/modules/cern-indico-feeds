# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.0.4] - 21/02/2025
- Add `cleanTemporaryFiles` and call before fetching any feed
- Always remove `$sink` in case of exception, avoiding `tmp` filling up

## [3.0.3] - 20/02/2025
- Prefix relative links with https://indico.cern.ch, fixing reported issues on home.cern.

## [3.0.2] - 27/11/2024
- changed `__construct(array $configuration, string $plugin_id, array $plugin_definition, ClientInterface $client, CacheBackendInterface $cache, FileSystemInterface $file_system)` to use less arguments and populate the client, cache and filesystem variables using the drupal service

## [3.0.1] - 01/11/2022
- Implement support for `FeedsCustomSource` and remove `abstract` from `IndicoParser.php`.
- Bump `composer.json` `feeds` requirement to `^3.0`.

## [3.0.0] - 14/10/2022
- Update module to ensure PHP 8.1 compatibility.

## [2.0.8] - 22/03/2022

- Update `api_key` configuration.

## [2.0.7] - 03/12/2021

- Remove `core: 8.x` from configuration files.

## [2.0.6] - 25/11/2021

- Add `core_version_requirement: ^9 || ^10` and remove `core: 8.x` from composer.

## [2.0.5] - 09/02/2021

- Add core: 8.x to cern_indico_events

## [2.0.4] - 09/02/2021

- Remove D9 deprecated function drupal_set_message (passes d9 scan)

## [2.0.3] - 08/02/2021

- Add core: 8.x to fix enabling issue

## [2.0.2] - 15/01/2021

- Update module to be D9-ready

## [2.0.1] - 21/05/2019

- Fixed issue with deprecated link l() function
